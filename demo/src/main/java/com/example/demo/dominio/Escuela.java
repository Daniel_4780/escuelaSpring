package com.example.demo.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Proyectos on 22/07/2017.
 */
@Entity
public class Escuela {
    private String NombreEscuela;
    private int CantidadMaestros;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    public Escuela() {
    }

    public Escuela(String nombreEscuela, int cantidadMaestros) {
        NombreEscuela = nombreEscuela;
        CantidadMaestros = cantidadMaestros;
    }

    public String getNombreEscuela() {
        return NombreEscuela;
    }

    public void setNombreEscuela(String nombreEscuela) {
        NombreEscuela = nombreEscuela;
    }

    public int getCantidadMaestros() {
        return CantidadMaestros;
    }

    public void setCantidadMaestros(int cantidadMaestros) {
        CantidadMaestros = cantidadMaestros;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
