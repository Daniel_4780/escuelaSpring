package com.example.demo.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Proyectos on 22/07/2017.
 */
@Entity
public class Maestro {
    private String NombreMaestro;
    private int EdadMaestro;
    private String EspecialidadMaestro;
    private long EscuelaId;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    public Maestro() {
    }

    public Maestro(String nombreMaestro, int edadMaestro, String especialidadMaestro, long escuelaId) {
        NombreMaestro = nombreMaestro;
        EdadMaestro = edadMaestro;
        EspecialidadMaestro = especialidadMaestro;
        EscuelaId = escuelaId;
    }

    public String getNombreMaestro() {
        return NombreMaestro;
    }

    public void setNombreMaestro(String nombreMaestro) {
        NombreMaestro = nombreMaestro;
    }

    public int getEdadMaestro() {
        return EdadMaestro;
    }

    public void setEdadMaestro(int edadMaestro) {
        EdadMaestro = edadMaestro;
    }

    public String getEspecialidadMaestro() {
        return EspecialidadMaestro;
    }

    public void setEspecialidadMaestro(String especialidadMaestro) {
        EspecialidadMaestro = especialidadMaestro;
    }

    public long getEscuelaId() {
        return EscuelaId;
    }

    public void setEscuelaId(long escuelaId) {
        EscuelaId = escuelaId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
