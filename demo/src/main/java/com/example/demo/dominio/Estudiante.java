package com.example.demo.dominio;

import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;
import org.springframework.boot.autoconfigure.web.ResourceProperties;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Proyectos on 22/07/2017.
 */

@Entity
public class Estudiante {
    private String Nombre;
    private int edad;

    @Id@GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Estudiante() {

    }

    public Estudiante(String nombre, int edad) {
        Nombre = nombre;
        this.edad = edad;
    }

    public String getNombre() {
        return Nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
}