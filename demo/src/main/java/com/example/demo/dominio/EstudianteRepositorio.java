package com.example.demo.dominio;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Proyectos on 22/07/2017.
 */
public interface EstudianteRepositorio extends JpaRepository<Estudiante, Long> {
}
