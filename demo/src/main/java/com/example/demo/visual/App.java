package com.example.demo.visual;

import com.example.demo.dominio.*;
import com.vaadin.event.ContextClickEvent;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Id;
import javax.xml.soap.Text;
import java.text.Normalizer;

/**
 * Created by Proyectos on 22/07/2017.
 */
@SpringUI
public class App extends UI{

    @Autowired
    EscuelaRepositorio repoEscuela;

    @Autowired
    EstudianteRepositorio repoEstudiante;

    @Autowired
    MaestroRepositorio repoMaestro;

    @Override
    protected void init(VaadinRequest vaadinRequest){
        VerticalLayout cuerpo = new VerticalLayout();

        //Contenido agregar escuela
        FormLayout agregarEscuela = new FormLayout();
        HorizontalLayout agregarEscuelaH = new HorizontalLayout();
        TextField nombreEscuela = new TextField("Nombre escuela");
        TextField cantidadMaestros = new TextField("Cantida maestros");
        Button btnAgregarEscuela = new Button("Agregar escuela");

        Grid<Escuela> escuelas = new Grid<>();
        escuelas.addColumn(Escuela::getId).setCaption("Correlativo");
        escuelas.addColumn(Escuela::getNombreEscuela).setCaption("Escuela");
        escuelas.addColumn(Escuela::getCantidadMaestros).setCaption("Cantida maestros");

        btnAgregarEscuela.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Escuela escuela = new Escuela();
                escuela.setNombreEscuela(nombreEscuela.getValue());
                escuela.setCantidadMaestros(Integer.parseInt(cantidadMaestros.getValue()));

                repoEscuela.save(escuela);
                escuelas.setItems(repoEscuela.findAll());

                nombreEscuela.clear();
                cantidadMaestros.clear();
                Notification.show("Escuela agregada con exito");
            }
        });

        btnAgregarEscuela.setStyleName("primary");
        agregarEscuelaH.addComponents(nombreEscuela, cantidadMaestros, btnAgregarEscuela);
        agregarEscuelaH.setComponentAlignment(btnAgregarEscuela, Alignment.BOTTOM_CENTER);
        agregarEscuela.addComponents(agregarEscuelaH, escuelas);

        //Cotenido de maestro
        FormLayout agregarMaestro = new FormLayout();
        HorizontalLayout agregarMaestroH = new HorizontalLayout();
        HorizontalLayout agregarMaestroH1 = new HorizontalLayout();

        TextField nombreMaestro = new TextField("Nombre maestro");
        TextField edadMaestro = new TextField("Edad");
        TextField especialidadMaestro = new TextField("Especialidad");
        ComboBox<Escuela> slcEscuela = new ComboBox<>();
        Button cargarEscuelas = new Button("Cargar escuelas");

        cargarEscuelas.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                slcEscuela.setItems(repoEscuela.findAll());
                slcEscuela.setItemCaptionGenerator(Escuela::getNombreEscuela);
            }
        });

        Grid<Maestro> maestros = new Grid<>();
        maestros.addColumn(Maestro::getId).setCaption("Correlativo");
        maestros.addColumn(Maestro::getNombreMaestro).setCaption("Nombre");
        maestros.addColumn(Maestro::getEdadMaestro).setCaption("Edad");

        Button btnAgregarMaestro = new Button("Agregar escuela");

        btnAgregarMaestro.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Maestro maestro = new Maestro()    ;
                maestro.setEdadMaestro(Integer.parseInt(edadMaestro.getValue()));
                Escuela escuela = slcEscuela.getSelectedItem().get();
                maestro.setEscuelaId(escuela.getId());
                maestro.setEspecialidadMaestro(especialidadMaestro.getValue());
                maestro.setNombreMaestro(nombreMaestro.getValue());
                repoMaestro.save(maestro);
                nombreMaestro.clear();
                edadMaestro.clear();
                especialidadMaestro.clear();
                maestros.setItems(repoMaestro.findAll());
                Notification.show("Se agrego exitosamente un maestro");
            }
        });
        btnAgregarMaestro.setStyleName("primary");
        agregarMaestroH.addComponents(nombreMaestro, edadMaestro, especialidadMaestro);
        agregarMaestroH1.addComponents(cargarEscuelas, slcEscuela, btnAgregarMaestro);
        agregarMaestro.addComponents(agregarMaestroH, agregarMaestroH1, maestros);


        //Contenido de estudiantes
        FormLayout agregarEstudiante = new FormLayout();
        HorizontalLayout agregarEstudianteH = new HorizontalLayout();

        TextField nombre = new TextField("Ingrese nombre");
        TextField edad = new TextField("Ingrese edad");
        Button agregar = new Button("Agregar estudiante");

        Grid<Estudiante> estudiante = new Grid<>();
        estudiante.addColumn(Estudiante::getId).setCaption("Correlativo");
        estudiante.addColumn(Estudiante::getNombre).setCaption("Nombre");
        estudiante.addColumn(Estudiante::getEdad).setCaption("Edad");

        agregar.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Estudiante alumno = new Estudiante();
                alumno.setNombre(nombre.getValue());
                alumno.setEdad(Integer.parseInt(edad.getValue()));
                repoEstudiante.save(alumno);

                estudiante.setItems(repoEstudiante.findAll());
                nombre.clear();
                edad.clear();

                Notification.show("Estudiante agregado");
            }
        });
        agregar.setStyleName("primary");
        agregarEstudianteH.addComponents(nombre, edad, agregar);
        agregarEstudianteH.setComponentAlignment(agregar, Alignment.BOTTOM_CENTER);
        agregarEstudiante.addComponents(agregarEstudianteH, estudiante);


        //Crear acordion
        Accordion accordion = new Accordion();
        Layout tabEscuela = new VerticalLayout();
        Layout tabMaestro = new VerticalLayout();
        Layout tabEstudiante = new VerticalLayout();
        tabEscuela.addComponent(agregarEscuela);
        tabMaestro.addComponent(agregarMaestro);
        tabEstudiante.addComponent(agregarEstudiante);
        accordion.addTab(tabEscuela, "Agregar escuela");
        accordion.addTab(tabMaestro,"Agregar maestros");
        accordion.addTab(tabEstudiante, "Agregar estudiante");
        cuerpo.addComponent(accordion);
        setContent(cuerpo);
    }
}
